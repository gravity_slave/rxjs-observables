import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'browser-event-experiments',
  templateUrl: './browser-event-experiments.component.html',
  styleUrls: ['./browser-event-experiments.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class BrowserEventExperimentsComponent implements OnInit {
  hoversection: HTMLElement;
  constructor() { }

  ngOnInit() {
    this.hoversection = document.getElementById('hover');
  }

}
