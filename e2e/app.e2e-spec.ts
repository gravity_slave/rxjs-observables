import { RxjsObsPage } from './app.po';

describe('rxjs-obs App', () => {
  let page: RxjsObsPage;

  beforeEach(() => {
    page = new RxjsObsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
